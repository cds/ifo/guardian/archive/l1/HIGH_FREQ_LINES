from guardian import GuardState
import time
import numpy as np
import configparser
import ifoconfig # load configuration file holding calirbation line amplitudes
#######################
request = 'WAIT_IN_LOW_NOISE' # Hanford had this set to 'FINISHED'
nominal = 'WAIT_IN_LOW_NOISE' # Hanford had this set to 'FINISHED'

###############################################
# reference values
###############################################
LINE_AMPLITUDE = 0.0
DICT_TIME = {'integrated_time' : 0}
ISC_LOCK_LOW_NOISE_STATE = 2000

###############################################
config = configparser.ConfigParser(comment_prefixes=('#',), inline_comment_prefixes=('#',))
config_file = open('/ligo/groups/cal/L1/ifo/pydarm_L1.ini')
config.read_file(config_file)
frequencies = np.array(config['high-frequency-roaming-lines']['FreqArray'].split(','))
FREQARRAY = frequencies.astype(float)

#FREQARRAY = np.array([4153.1, 3653.1, 3153.1, 2653.1, 2153.1, 1653.1, 1153.1])
TIMEARRAY = np.array([8, 8, 5, 5, 2, 2, 2])
###############################################

class INIT(GuardState):
    index = 0
    def main(self):
        if ifoconfig.use_pcaly == 1 and ifoconfig.pcalx_good == 1:
            log('Setting the initial freq')
            ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = FREQARRAY[0]
            ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = LINE_AMPLITUDE
            ezca['CAL-PCALX_PCALOSC1_OSC_COSGAIN'] = LINE_AMPLITUDE
            ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 3.0
            DICT_TIME['integrated_time']  = 0
            return 'IDLE'
        else:
            log('Not Setting the initial freq due to loss of a Pcal')
            return 'IDLE'


class IDLE(GuardState):
    """Idle state to do nothing, off state
    """
    index = 2
    goto=True

    def main(self):
        log('Frequency array is '+str(FREQARRAY))
        return True
    def run(self):
        return True

class CHANGE_FREQ_IF_NEEDED(GuardState):
    index = 10
    def main(self):
        if ifoconfig.use_pcaly == 1 and ifoconfig.pcalx_good == 1:
            try:
                self.CUR_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ']
                self.CUR_IND = np.where(FREQARRAY == round(self.CUR_FREQ,1))[0][0]
            except:
                notify('Error checking current frequency')
                return 'INIT'
        else:
            return True

    def run(self):
        if ifoconfig.use_pcaly == 1 and ifoconfig.pcalx_good == 1:
            if ezca['GRD-ISC_LOCK_STATE_N'] > 11:
                log('return true')
                return True
            elif DICT_TIME['integrated_time'] >= TIMEARRAY[self.CUR_IND]*3601:
                log('Integrated time = ' + str(DICT_TIME['integrated_time']) + ',requested time = ' + str(TIMEARRAY[self.CUR_IND]*3601))
                DICT_TIME['integrated_time'] = 0
                self.CUR_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ']
                if (self.CUR_FREQ - 500) > 1000:
                    NEW_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] - 500
                    ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = NEW_FREQ
                    self.CUR_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ']
                    try:
                        self.CUR_IND = np.where(FREQARRAY == round(self.CUR_FREQ,1))[0][0]
                    except:
                        log('Frequency did not match array, resetting to first frequency')
                        self.CUR_FREQ = FREQARRAY[0]
                        self.CUR_IND = 0
                        ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = self.CUR_FREQ
                    log('Changing frequency to ' + str(NEW_FREQ) + ' Hz')
                else:
                    self.CUR_FREQ = FREQARRAY[0]
                    self.CUR_IND = 0
                    ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = self.CUR_FREQ
                    log('End of time array, going back to first frequency: ' + str(self.CUR_FREQ))
        else:
            return True

class WAIT_IN_LOW_NOISE(GuardState):
    index = 20
    def main(self):
        self.arrived_low_noise = False
        self.start_of_lock = None

    def run(self):
        if ifoconfig.use_pcaly == 1 and ifoconfig.pcalx_good == 1:
            if not self.arrived_low_noise:
                if ezca['GRD-ISC_LOCK_STATE_N'] == ISC_LOCK_LOW_NOISE_STATE:
                    self.arrived_low_noise = True
                    self.start_of_lock = ezca['FEC-124_TIME_DIAG']
                    log('Arrived in low noise')
            if ezca['GRD-ISC_LOCK_STATE_N'] <= 11:
                if self.start_of_lock != None:
                    DICT_TIME['integrated_time'] = DICT_TIME['integrated_time'] + (ezca['FEC-124_TIME_DIAG'] - self.start_of_lock)
                    #Quick sanity check in case epics channels go bad and we end up with a negative 40 years or something in seconds
                    if DICT_TIME['integrated_time'] < 0:
                        DICT_TIME['integrated_time'] = 0
                    log('Integrated seconds so far: ' + str(DICT_TIME['integrated_time']))
                return 'IDLE'
            time.sleep(0.5)
            return True
        else:
            return True


######################
edges = [
    ('IDLE', 'CHANGE_FREQ_IF_NEEDED'),
    ('CHANGE_FREQ_IF_NEEDED','WAIT_IN_LOW_NOISE'),
    ]
